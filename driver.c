#include <stdio.h>

#include "ticker.h"

int main(int argc, char* argv[]){

	FILE *fp;

	if(argc != 2){
		printf("Not enough on the command line\n");
		return 0;
	}	
	else{
		fp = fopen(argv[1], "r");
		if(!fp){
			printf("No file\n");
			return 0;
		}
	}

	char symbol[6];
	double cents = 0;
	char name[65];
	char symbol2[6];
	double cents2 = 0;
	char name2[65];

	market *m = market_create(compare_symbol);
	while(!feof(fp)){	
		name[0] = '\0';
		if(2 != fscanf(fp, "%s %lf", symbol, &cents)){
			break;
		}

		if(fgetc(fp) == ' '){
			fscanf(fp, "%[^\n]", name);
		}
		else if(fgetc(fp) == EOF){
			break;
		}

		struct company *comp = stock_create(symbol, name, cents);
		market_insert(m, comp);
	}

	tree_inorder(m->root);
	printf("\n");
	
	name2[0] = '\0';
	while(!feof(stdin)){
		cents2 = 0;
		fscanf(stdin, "%s %lf", symbol2, &cents2);	
		struct company *comp2 = stock_create(symbol2, name2, cents2);
		market_insert(m, comp2);
	}
	
	printf("\n");
	tree_inorder(m->root);

	char symbol3[6] = "temp";
	char symbol4[6] = "temp";

	market *dst_m = market_create(compare_cents);
	struct company *c =  stock_create(symbol3, symbol4, 10);
	struct tree *z = tree_create(c);
	dst_m->root = z; 
	market_copy(dst_m, m->root);

	printf("\n");
	tree_inorder(dst_m->root);

	//free(c);
	//free(z);
	market_destroy(m);
	market_dissassemble(dst_m);
	
	fclose(fp);
	return 0;
}






