
#include "ticker.h"

//Provided code
struct tree *tree_create(struct company *data){
	struct tree *t = malloc(sizeof(*t));
	if(t) {
		t->data = data;
		t->left = NULL;
		t->right = NULL;
	}

	return t;
}

//Function that creates a market
market *market_create(int (*cmp)(const struct company *, const struct company *)){
	market *m = malloc(sizeof(*m));	
	if(m) {
		m->cmp = cmp;
		m->root = NULL;
	}
	
	return m;
}

//Function that inserts a tree into a market
market *market_insert(market *m, struct company *comp){
	if(!m->root){
		m->root = tree_create(comp);
	}
	else{
		tree_insert(m->root, comp, m->cmp);
	}
	return m;
}

//Function that destorys a market by recursively calling itself
void market_destroy(market *m){
	if(!m) {
		return;
	}
	tree_destroy(m->root);
	free(m);
}

//Function that is called in market_destroy recursively
void tree_destroy(struct tree *t){
	if(!t) {
		return;
	}

	tree_destroy(t->left);
	tree_destroy(t->right);
	free(t->data->name);
	free(t->data);
	free(t);
}

//Function that compares the symbols of the companies
int compare_symbol(const struct company *a, const struct company *b){
	if(a && b){
		return strcmp(a->symbol, b->symbol);
	}
	return 0;

}

//Functioni that compares the cents of the companies
int compare_cents(const struct company *a, const struct company *b){
	if(a && b){
		if(a->cents < b->cents){
			return -1;
		}
		else if(a->cents == b->cents){
			return 0;
		}
		else{
			return 1;
		}
	}	
	return 0;
}

//Provided code with the exception of the freeing and the checking for the price of the stock in the else if statement
bool tree_insert(struct tree *t, struct company *comp, int (*cmp)(const struct company *, const struct company *)){
	if(cmp(comp, t->data) < 0){
		if(t->left){
			return tree_insert(t->left, comp, cmp);
		}
		else{
			t->left = tree_create(comp);
			return t->left;
		}
	}
	else if (cmp(comp, t->data) == 0){
		if((t->data->cents + comp->cents) > 1){
			t->data->cents += comp->cents;
			return t->data->cents;
		}
		else{
			free(comp->name);
			free(comp);
			return fprintf(stderr, "Not valid input\n");
		}
	}
	else{
		if(t->right){
			return tree_insert(t->right, comp, cmp);
		}
		else{
			t->right = tree_create(comp);
			return t->right;
		}
	}
	return false;
}

//Provided code that creates a new company
struct company *stock_create(char *symbol, char *name, double price){
	struct company *new_stock = malloc(sizeof(*new_stock));
	if(!new_stock){
		return NULL;
	}

	new_stock->name = strdup(name);
	if(!new_stock->name){
		free(new_stock);
		return NULL;
	}

	strncpy(new_stock->symbol, symbol, sizeof(new_stock->symbol)-1);
	new_stock->symbol[sizeof(new_stock->symbol)-1] = '\0';

	new_stock->cents = 100 * price;

	return new_stock;
}

//Function that loops through a tree and prints it out to the screen
void tree_inorder(struct tree *t){
	if(!t) {
		return;
	}
	
	tree_inorder(t->left);
	printf("%s %zd.%zd %s\n", t->data->symbol, t->data->cents / 100, t->data->cents % 100, t->data->name);
	tree_inorder(t->right);
}

//Function that copies a market by using tree_insert and market_copy functions
void market_copy(market *dst_m, struct tree *src){
	if(!src) {
		return;
	}
	
	market_insert(dst_m, src->data);
	market_copy(dst_m, src->left);
	market_copy(dst_m, src->right);
}

//Function that dissassembles a tree recursively 
void tree_dissassemble(struct tree *t){
	if(!t){
		return;
	}

	tree_dissassemble(t->left);
	t->left = NULL;
	tree_dissassemble(t->right);
	t->right = NULL;
	free(t);
}

//Function that dissassembles a market recursively 
void market_dissassemble(market *m){
	if(!m){
		return;
	}
	tree_dissassemble(m->root);
	free(m);
}

int main(int argc, char* argv[]){

	FILE *fp;

	//Checking to make sure only two arguments are entered on the commandline and if there are two then to open a file 
	if(argc != 2){
		printf("Not enough on the command line\n");
		return 0;
	}	
	else{
		fp = fopen(argv[1], "r");
		if(!fp){
			printf("No file\n");
			return 0;
		}
	}

	char symbol[6];
	double cents = 0;
	char name[65];
	char symbol2[6];
	double cents2 = 0;
	char name2[65];

	//Create a market and then loop through a file scanning the data and storing it in the newly created market
	market *m = market_create(compare_symbol);
	while(!feof(fp)){	
		name[0] = '\0';
		if(2 != fscanf(fp, "%5s %lf", symbol, &cents)){
			if(feof(fp)){
				break;
			}
			else{
				fprintf(stderr, "exiting program\n");
				market_destroy(m);
				fclose(fp);
				exit(1);
			}
		}

		if(fgetc(fp) == ' '){
			fscanf(fp, "%[^\n]", name);
		}
		else if(fgetc(fp) == EOF){
			break;
		}

		struct company *comp = stock_create(symbol, name, cents);
		market_insert(m, comp);
	}

	//Get user input until ctrl d is entered and alter the cents of each company based upon the user input
	name2[0] = '\0';
	while(!feof(stdin)){
		int ch;
		cents2 = 0;
		if(2 != fscanf(stdin, "%5s %lf", symbol2, &cents2)){
			continue;
		}
		else{
			while((ch = getchar()) != '\n' && ch != EOF);
		}
		struct company *comp2 = stock_create(symbol2, name2, cents2);
		market_insert(m, comp2);
	}
	
	//Create a second market that will have the data from the first market copied over, except it will be stored 
	//lowest to highest based upon the cents
	market *dst_m = market_create(compare_cents);
	market_copy(dst_m, m->root);

	//Print out the data in the second market
	printf("\n");
	tree_inorder(dst_m->root);

	//Destory the first market, dissasseble the second market, and close the file pointer
	market_destroy(m);
	market_dissassemble(dst_m);
	
	fclose(fp);
	return 0;
}







